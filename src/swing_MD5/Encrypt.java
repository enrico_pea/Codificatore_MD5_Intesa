package swing_MD5;

import java.security.MessageDigest;

public class Encrypt {

	public static String HashCrypt(String password) throws Exception {
		// String password = "123456";

		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(password.getBytes());

		byte byteData[] = md.digest();

		// convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}

		/*
		 * System.out.println("Password in chiaro: " + password);
		 * System.out.println("Password codificata MD5: " + sb.toString());
		 */

		return sb.toString();

	}

}
